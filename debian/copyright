Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vertx-codegen
Source: https://github.com/eclipse-vertx/vertx-codegen
Files-Excluded: src/tck/generated src/converters/generated

Files: *
Copyright: 2014-2021, Red Hat, Inc
License: EPL-1.0 OR Apache-2.0

Files: src/converters/*
       src/main/java/io/vertx/core/json/impl/JsonUtil.java
       src/test/java/io/vertx/test/codegen/converter/DataObjectTest.java
Copyright: 2011-2019, Contributors to the Eclipse Foundation
License: EPL-2.0 OR Apache-2.0

Files: src/test/java/io/vertx/test/codegen/ClassEnumerator.java
Copyright: StackOverflow Contributors
License: CC-BY-SA-2.5 AND WTFPL

Files: src/test/java/io/vertx/test/codegen/converter/TestUtils.java
Copyright: 2014, Red Hat, Inc. and others
License: EPL-2.0 OR Apache-2.0

Files: debian/*
Copyright: 2023, Joseph Nahmias <jello@debian.org>
License: Apache-2.0

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: CC-BY-SA-2.5
 THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE
 COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY
 COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS
 AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
 .
 BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE
 TO BE BOUND BY THE TERMS OF THIS LICENSE. THE LICENSOR GRANTS YOU THE
 RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS
 AND CONDITIONS.
 .
 1. Definitions
 .
     a. "Collective Work" means a work, such as a periodical issue,
     anthology or encyclopedia, in which the Work in its entirety in
     unmodified form, along with a number of other contributions,
     constituting separate and independent works in themselves, are
     assembled into a collective whole. A work that constitutes a
     Collective Work will not be considered a Derivative Work (as defined
     below) for the purposes of this License.
 .
     b. "Derivative Work" means a work based upon the Work
     or upon the Work and other pre-existing works, such as a translation,
     musical arrangement, dramatization, fictionalization, motion picture
     version, sound recording, art reproduction, abridgment, condensation,
     or any other form in which the Work may be recast, transformed, or
     adapted, except that a work that constitutes a Collective Work will
     not be considered a Derivative Work for the purpose of this License.
     For the avoidance of doubt, where the Work is a musical composition
     or sound recording, the synchronization of the Work in timed-relation
     with a moving image ("synching") will be considered a Derivative Work
     for the purpose of this License.
 .
     c. "Licensor" means the individual or entity that offers the Work under
     the terms of this License.
 .
     d. "Original Author" means the individual or entity who created
     the Work.
 .
     e. "Work" means the copyrightable work of authorship offered under the
     terms of this License.
 .
     f. "You" means an individual or entity exercising rights under this
     License who has not previously violated the terms of this License
     with respect to the Work, or who has received express permission from
     the Licensor to exercise rights under this License despite a previous
     violation.
 .
     g. "License Elements" means the following high-level license
     attributes as selected by Licensor and indicated in the title of this
     License: Attribution, ShareAlike.
 .
 2. Fair Use Rights. Nothing in this license is intended to reduce, limit,
 or restrict any rights arising from fair use, first sale or other
 limitations on the exclusive rights of the copyright owner under
 copyright law or other applicable laws.
 .
 3. License Grant. Subject to the terms and conditions of this License,
 Licensor hereby grants You a worldwide, royalty-free, non-exclusive,
 perpetual (for the duration of the applicable copyright) license to
 exercise the rights in the Work as stated below:
 .
     a. to reproduce the Work, to incorporate the Work into one or more
     Collective Works, and to reproduce the Work as incorporated in the
     Collective Works;
 .
     b. to create and reproduce Derivative Works;
 .
     c. to distribute copies or phonorecords of, display publicly, perform
     publicly, and perform publicly by means of a digital audio
     transmission the Work including as incorporated in Collective Works;
 .
     d. to distribute copies or phonorecords of, display publicly, perform
     publicly, and perform publicly by means of a digital audio
     transmission Derivative Works.
 .
     e. For the avoidance of doubt, where the work is a musical composition:
 .
         i. Performance Royalties Under Blanket Licenses. Licensor waives
         the exclusive right to collect, whether individually or via a
         performance rights society (e.g. ASCAP, BMI, SESAC), royalties
         for the public performance or public digital performance (e.g.
         webcast) of the Work.
 .
         ii. Mechanical Rights and Statutory Royalties. Licensor waives
         the exclusive right to collect, whether individually or via a
         music rights society or designated agent (e.g. Harry Fox Agency),
         royalties for any phonorecord You create from the Work ("cover
         version") and distribute, subject to the compulsory license
         created by 17 USC Section 115 of the US Copyright Act (or the
         equivalent in other jurisdictions).
 .
     f. Webcasting Rights and Statutory Royalties. For the avoidance of
     doubt, where the Work is a sound recording, Licensor waives the
     exclusive right to collect, whether individually or via a
     performance-rights society (e.g. SoundExchange), royalties for the
     public digital performance (e.g. webcast) of the Work, subject to the
     compulsory license created by 17 USC Section 114 of the US Copyright
     Act (or the equivalent in other jurisdictions).
 .
 The above rights may be exercised in all media and formats whether now
 known or hereafter devised. The above rights include the right to make
 such modifications as are technically necessary to exercise the rights in
 other media and formats. All rights not expressly granted by Licensor are
 hereby reserved.
 .
 4. Restrictions.The license granted in Section 3 above is expressly made
 subject to and limited by the following restrictions:
 .
     a. You may distribute, publicly display, publicly perform, or
     publicly digitally perform the Work only under the terms of this
     License, and You must include a copy of, or the Uniform Resource
     Identifier for, this License with every copy or phonorecord of the
     Work You distribute, publicly display, publicly perform, or publicly
     digitally perform. You may not offer or impose any terms on the Work
     that alter or restrict the terms of this License or the recipients'
     exercise of the rights granted hereunder. You may not sublicense the
     Work. You must keep intact all notices that refer to this License and
     to the disclaimer of warranties. You may not distribute, publicly
     display, publicly perform, or publicly digitally perform the Work
     with any technological measures that control access or use of the
     Work in a manner inconsistent with the terms of this License
     Agreement. The above applies to the Work as incorporated in a
     Collective Work, but this does not require the Collective Work apart
     from the Work itself to be made subject to the terms of this License.
     If You create a Collective Work, upon notice from any Licensor You
     must, to the extent practicable, remove from the Collective Work any
     credit as required by clause 4(c), as requested. If You create a
     Derivative Work, upon notice from any Licensor You must, to the
     extent practicable, remove from the Derivative Work any credit as
     required by clause 4(c), as requested.
 .
     b. You may distribute, publicly display,
     publicly perform, or publicly digitally perform a Derivative Work
     only under the terms of this License, a later version of this License
     with the same License Elements as this License, or a Creative Commons
     iCommons license that contains the same License Elements as this
     License (e.g. Attribution-ShareAlike 2.5 Japan). You must include a
     copy of, or the Uniform Resource Identifier for, this License or
     other license specified in the previous sentence with every copy or
     phonorecord of each Derivative Work You distribute, publicly display,
     publicly perform, or publicly digitally perform. You may not offer or
     impose any terms on the Derivative Works that alter or restrict the
     terms of this License or the recipients' exercise of the rights
     granted hereunder, and You must keep intact all notices that refer to
     this License and to the disclaimer of warranties. You may not
     distribute, publicly display, publicly perform, or publicly digitally
     perform the Derivative Work with any technological measures that
     control access or use of the Work in a manner inconsistent with the
     terms of this License Agreement. The above applies to the Derivative
     Work as incorporated in a Collective Work, but this does not require
     the Collective Work apart from the Derivative Work itself to be made
     subject to the terms of this License.
 .
     c. If you distribute, publicly display, publicly perform, or publicly
     digitally perform the Work or any Derivative Works or Collective
     Works, You must keep intact all copyright notices for the Work and
     provide, reasonable to the medium or means You are utilizing: (i) the
     name of the Original Author (or pseudonym, if applicable) if
     supplied, and/or (ii) if the Original Author and/or Licensor
     designate another party or parties (e.g. a sponsor institute,
     publishing entity, journal) for attribution in Licensor's copyright
     notice, terms of service or by other reasonable means, the name of
     such party or parties; the title of the Work if supplied; to the
     extent reasonably practicable, the Uniform Resource Identifier, if
     any, that Licensor specifies to be associated with the Work, unless
     such URI does not refer to the copyright notice or licensing
     information for the Work; and in the case of a Derivative Work, a
     credit identifying the use of the Work in the Derivative Work (e.g.,
     "French translation of the Work by Original Author," or "Screenplay
     based on original Work by Original Author"). Such credit may be
     implemented in any reasonable manner; provided, however, that in the
     case of a Derivative Work or Collective Work, at a minimum such
     credit will appear where any other comparable authorship credit
     appears and in a manner at least as prominent as such other
     comparable authorship credit.
 .
 5. Representations, Warranties and Disclaimer
 .
 UNLESS OTHERWISE AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE
 WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND
 CONCERNING THE MATERIALS, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE,
 INCLUDING, WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY,
 FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF
 LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS,
 WHETHER OR NOT DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE
 EXCLUSION OF IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
 .
 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE
 LAW, IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR
 ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES
 ARISING OUT OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS
 BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 7. Termination
 .
     a. This License and the rights granted hereunder will terminate
     automatically upon any breach by You of the terms of this License.
     Individuals or entities who have received Derivative Works or
     Collective Works from You under this License, however, will not have
     their licenses terminated provided such individuals or entities
     remain in full compliance with those licenses. Sections 1, 2, 5, 6,
     7, and 8 will survive any termination of this License.
 .
     b. Subject to the above terms and conditions, the license granted
     here is perpetual (for the duration of the applicable copyright in
     the Work).  Notwithstanding the above, Licensor reserves the right to
     release the Work under different license terms or to stop
     distributing the Work at any time; provided, however that any such
     election will not serve to withdraw this License (or any other
     license that has been, or is required to be, granted under the terms
     of this License), and this License will continue in full force and
     effect unless terminated as stated above.
 .
 8. Miscellaneous
 .
     a. Each time You distribute or publicly digitally perform the Work or a
     Collective Work, the Licensor offers to the recipient a license to
     the Work on the same terms and conditions as the license granted to
     You under this License.
 .
     b. Each time You distribute or publicly digitally perform a
     Derivative Work, Licensor offers to the recipient a license to the
     original Work on the same terms and conditions as the license granted
     to You under this License.
 .
     c. If any provision of this License is invalid or unenforceable under
     applicable law, it shall not affect the validity or enforceability of
     the remainder of the terms of this License, and without further
     action by the parties to this agreement, such provision shall be
     reformed to the minimum extent necessary to make such provision valid
     and enforceable.
 .
     d. No term or provision of this License shall be deemed waived and no
     breach consented to unless such waiver or consent shall be in writing
     and signed by the party to be charged with such waiver or consent.
 .
     e. This License constitutes the entire agreement between the parties
     with respect to the Work licensed here. There are no understandings,
     agreements or representations with respect to the Work not specified
     here. Licensor shall not be bound by any additional provisions that
     may appear in any communication from You. This License may not be
     modified without the mutual written agreement of the Licensor and
     You.

License: EPL-1.0
 Eclipse Public License - v 1.0
 .
 THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE ("AGREEMENT"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
 .
 1. DEFINITIONS
 .
 "Contribution" means:
 .
 a) in the case of the initial Contributor, the initial code and documentation distributed under this Agreement, and
 .
 b) in the case of each subsequent Contributor:
 .
 i) changes to the Program, and
 .
 ii) additions to the Program;
 .
 where such changes and/or additions to the Program originate from and are distributed by that particular Contributor. A Contribution 'originates' from a Contributor if it was added to the Program by such Contributor itself or anyone acting on such Contributor's behalf. Contributions do not include additions to the Program which: (i) are separate modules of software distributed in conjunction with the Program under their own license agreement, and (ii) are not derivative works of the Program.
 .
 "Contributor" means any person or entity that distributes the Program.
 .
 "Licensed Patents" mean patent claims licensable by a Contributor which are necessarily infringed by the use or sale of its Contribution alone or when combined with the Program.
 .
 "Program" means the Contributions distributed in accordance with this Agreement.
 .
 "Recipient" means anyone who receives the Program under this Agreement, including all Contributors.
 .
 2. GRANT OF RIGHTS
 .
 a) Subject to the terms of this Agreement, each Contributor hereby grants Recipient a non-exclusive, worldwide, royalty-free copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, distribute and sublicense the Contribution of such Contributor, if any, and such derivative works, in source code and object code form.
 .
 b) Subject to the terms of this Agreement, each Contributor hereby grants Recipient a non-exclusive, worldwide, royalty-free patent license under Licensed Patents to make, use, sell, offer to sell, import and otherwise transfer the Contribution of such Contributor, if any, in source code and object code form. This patent license shall apply to the combination of the Contribution and the Program if, at the time the Contribution is added by the Contributor, such addition of the Contribution causes such combination to be covered by the Licensed Patents. The patent license shall not apply to any other combinations which include the Contribution. No hardware per se is licensed hereunder.
 .
 c) Recipient understands that although each Contributor grants the licenses to its Contributions set forth herein, no assurances are provided by any Contributor that the Program does not infringe the patent or other intellectual property rights of any other entity. Each Contributor disclaims any liability to Recipient for claims brought by any other entity based on infringement of intellectual property rights or otherwise. As a condition to exercising the rights and licenses granted hereunder, each Recipient hereby assumes sole responsibility to secure any other intellectual property rights needed, if any. For example, if a third party patent license is required to allow Recipient to distribute the Program, it is Recipient's responsibility to acquire that license before distributing the Program.
 .
 d) Each Contributor represents that to its knowledge it has sufficient copyright rights in its Contribution, if any, to grant the copyright license set forth in this Agreement.
 .
 3. REQUIREMENTS
 .
 A Contributor may choose to distribute the Program in object code form under its own license agreement, provided that:
 .
 a) it complies with the terms and conditions of this Agreement; and
 .
 b) its license agreement:
 .
 i) effectively disclaims on behalf of all Contributors all warranties and conditions, express and implied, including warranties or conditions of title and non-infringement, and implied warranties or conditions of merchantability and fitness for a particular purpose;
 .
 ii) effectively excludes on behalf of all Contributors all liability for damages, including direct, indirect, special, incidental and consequential damages, such as lost profits;
 .
 iii) states that any provisions which differ from this Agreement are offered by that Contributor alone and not by any other party; and
 .
 iv) states that source code for the Program is available from such Contributor, and informs licensees how to obtain it in a reasonable manner on or through a medium customarily used for software exchange.
 .
 When the Program is made available in source code form:
 .
 a) it must be made available under this Agreement; and
 .
 b) a copy of this Agreement must be included with each copy of the Program.
 .
 Contributors may not remove or alter any copyright notices contained within the Program.
 .
 Each Contributor must identify itself as the originator of its Contribution, if any, in a manner that reasonably allows subsequent Recipients to identify the originator of the Contribution.
 .
 4. COMMERCIAL DISTRIBUTION
 .
 Commercial distributors of software may accept certain responsibilities with respect to end users, business partners and the like. While this license is intended to facilitate the commercial use of the Program, the Contributor who includes the Program in a commercial product offering should do so in a manner which does not create potential liability for other Contributors. Therefore, if a Contributor includes the Program in a commercial product offering, such Contributor ("Commercial Contributor") hereby agrees to defend and indemnify every other Contributor ("Indemnified Contributor") against any losses, damages and costs (collectively "Losses") arising from claims, lawsuits and other legal actions brought by a third party against the Indemnified Contributor to the extent caused by the acts or omissions of such Commercial Contributor in connection with its distribution of the Program in a commercial product offering. The obligations in this section do not apply to any claims or Losses relating to any actual or alleged intellectual property infringement. In order to qualify, an Indemnified Contributor must: a) promptly notify the Commercial Contributor in writing of such claim, and b) allow the Commercial Contributor to control, and cooperate with the Commercial Contributor in, the defense and any related settlement negotiations. The Indemnified Contributor may participate in any such claim at its own expense.
 .
 For example, a Contributor might include the Program in a commercial product offering, Product X. That Contributor is then a Commercial Contributor. If that Commercial Contributor then makes performance claims, or offers warranties related to Product X, those performance claims and warranties are such Commercial Contributor's responsibility alone. Under this section, the Commercial Contributor would have to defend claims against the other Contributors related to those performance claims and warranties, and if a court requires any other Contributor to pay any damages as a result, the Commercial Contributor must pay those damages.
 .
 5. NO WARRANTY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, THE PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Each Recipient is solely responsible for determining the appropriateness of using and distributing the Program and assumes all risks associated with its exercise of rights under this Agreement , including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and unavailability or interruption of operations.
 .
 6. DISCLAIMER OF LIABILITY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, NEITHER RECIPIENT NOR ANY CONTRIBUTORS SHALL HAVE ANY LIABILITY FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION LOST PROFITS), HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OR DISTRIBUTION OF THE PROGRAM OR THE EXERCISE OF ANY RIGHTS GRANTED HEREUNDER, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 7. GENERAL
 .
 If any provision of this Agreement is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this Agreement, and without further action by the parties hereto, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable.
 .
 If Recipient institutes patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Program itself (excluding combinations of the Program with other software or hardware) infringes such Recipient's patent(s), then such Recipient's rights granted under Section 2(b) shall terminate as of the date such litigation is filed.
 .
 All Recipient's rights under this Agreement shall terminate if it fails to comply with any of the material terms or conditions of this Agreement and does not cure such failure in a reasonable period of time after becoming aware of such noncompliance. If all Recipient's rights under this Agreement terminate, Recipient agrees to cease use and distribution of the Program as soon as reasonably practicable. However, Recipient's obligations under this Agreement and any licenses granted by Recipient relating to the Program shall continue and survive.
 .
 Everyone is permitted to copy and distribute copies of this Agreement, but in order to avoid inconsistency the Agreement is copyrighted and may only be modified in the following manner. The Agreement Steward reserves the right to publish new versions (including revisions) of this Agreement from time to time. No one other than the Agreement Steward has the right to modify this Agreement. The Eclipse Foundation is the initial Agreement Steward. The Eclipse Foundation may assign the responsibility to serve as the Agreement Steward to a suitable separate entity. Each new version of the Agreement will be given a distinguishing version number. The Program (including Contributions) may always be distributed subject to the version of the Agreement under which it was received. In addition, after a new version of the Agreement is published, Contributor may elect to distribute the Program (including its Contributions) under the new version. Except as expressly stated in Sections 2(a) and 2(b) above, Recipient receives no rights or licenses to the intellectual property of any Contributor under this Agreement, whether expressly, by implication, estoppel or otherwise. All rights in the Program not expressly granted under this Agreement are reserved.
 .
 This Agreement is governed by the laws of the State of New York and the intellectual property laws of the United States of America. No party to this Agreement will bring a legal action under this Agreement more than one year after the cause of action arose. Each party waives its rights to a jury trial in any resulting litigation.

License: EPL-2.0
 Eclipse Public License - v 2.0
 .
 THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE (“AGREEMENT”). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
 1. DEFINITIONS
 .
 “Contribution” means:
 .
     a) in the case of the initial Contributor, the initial content Distributed under this Agreement, and
     b) in the case of each subsequent Contributor:
         i) changes to the Program, and
         ii) additions to the Program;
     where such changes and/or additions to the Program originate from and are Distributed by that particular Contributor. A Contribution “originates” from a Contributor if it was added to the Program by such Contributor itself or anyone acting on such Contributor's behalf. Contributions do not include changes or additions to the Program that are not Modified Works.
 .
 “Contributor” means any person or entity that Distributes the Program.
 .
 “Licensed Patents” mean patent claims licensable by a Contributor which are necessarily infringed by the use or sale of its Contribution alone or when combined with the Program.
 .
 “Program” means the Contributions Distributed in accordance with this Agreement.
 .
 “Recipient” means anyone who receives the Program under this Agreement or any Secondary License (as applicable), including Contributors.
 .
 “Derivative Works” shall mean any work, whether in Source Code or other form, that is based on (or derived from) the Program and for which the editorial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work of authorship.
 .
 “Modified Works” shall mean any work in Source Code or other form that results from an addition to, deletion from, or modification of the contents of the Program, including, for purposes of clarity any new file in Source Code form that contains any contents of the Program. Modified Works shall not include works that contain only declarations, interfaces, types, classes, structures, or files of the Program solely in each case in order to link to, bind by name, or subclass the Program or Modified Works thereof.
 .
 “Distribute” means the acts of a) distributing or b) making available in any manner that enables the transfer of a copy.
 .
 “Source Code” means the form of a Program preferred for making modifications, including but not limited to software source code, documentation source, and configuration files.
 .
 “Secondary License” means either the GNU General Public License, Version 2.0, or any later versions of that license, including any exceptions or additional permissions as identified by the initial Contributor.
 2. GRANT OF RIGHTS
 .
     a) Subject to the terms of this Agreement, each Contributor hereby grants Recipient a non-exclusive, worldwide, royalty-free copyright license to reproduce, prepare Derivative Works of, publicly display, publicly perform, Distribute and sublicense the Contribution of such Contributor, if any, and such Derivative Works.
     b) Subject to the terms of this Agreement, each Contributor hereby grants Recipient a non-exclusive, worldwide, royalty-free patent license under Licensed Patents to make, use, sell, offer to sell, import and otherwise transfer the Contribution of such Contributor, if any, in Source Code or other form. This patent license shall apply to the combination of the Contribution and the Program if, at the time the Contribution is added by the Contributor, such addition of the Contribution causes such combination to be covered by the Licensed Patents. The patent license shall not apply to any other combinations which include the Contribution. No hardware per se is licensed hereunder.
     c) Recipient understands that although each Contributor grants the licenses to its Contributions set forth herein, no assurances are provided by any Contributor that the Program does not infringe the patent or other intellectual property rights of any other entity. Each Contributor disclaims any liability to Recipient for claims brought by any other entity based on infringement of intellectual property rights or otherwise. As a condition to exercising the rights and licenses granted hereunder, each Recipient hereby assumes sole responsibility to secure any other intellectual property rights needed, if any. For example, if a third party patent license is required to allow Recipient to Distribute the Program, it is Recipient's responsibility to acquire that license before distributing the Program.
     d) Each Contributor represents that to its knowledge it has sufficient copyright rights in its Contribution, if any, to grant the copyright license set forth in this Agreement.
     e) Notwithstanding the terms of any Secondary License, no Contributor makes additional grants to any Recipient (other than those set forth in this Agreement) as a result of such Recipient's receipt of the Program under the terms of a Secondary License (if permitted under the terms of Section 3).
 .
 3. REQUIREMENTS
 .
 3.1 If a Contributor Distributes the Program in any form, then:
 .
     a) the Program must also be made available as Source Code, in accordance with section 3.2, and the Contributor must accompany the Program with a statement that the Source Code for the Program is available under this Agreement, and informs Recipients how to obtain it in a reasonable manner on or through a medium customarily used for software exchange; and
     b) the Contributor may Distribute the Program under a license different than this Agreement, provided that such license:
         i) effectively disclaims on behalf of all other Contributors all warranties and conditions, express and implied, including warranties or conditions of title and non-infringement, and implied warranties or conditions of merchantability and fitness for a particular purpose;
         ii) effectively excludes on behalf of all other Contributors all liability for damages, including direct, indirect, special, incidental and consequential damages, such as lost profits;
         iii) does not attempt to limit or alter the recipients' rights in the Source Code under section 3.2; and
         iv) requires any subsequent distribution of the Program by any party to be under a license that satisfies the requirements of this section 3.
 .
 3.2 When the Program is Distributed as Source Code:
 .
     a) it must be made available under this Agreement, or if the Program (i) is combined with other material in a separate file or files made available under a Secondary License, and (ii) the initial Contributor attached to the Source Code the notice described in Exhibit A of this Agreement, then the Program may be made available under the terms of such Secondary Licenses, and
     b) a copy of this Agreement must be included with each copy of the Program.
 .
 3.3 Contributors may not remove or alter any copyright, patent, trademark, attribution notices, disclaimers of warranty, or limitations of liability (‘notices’) contained within the Program from any copy of the Program which they Distribute, provided that Contributors may add their own appropriate notices.
 4. COMMERCIAL DISTRIBUTION
 .
 Commercial distributors of software may accept certain responsibilities with respect to end users, business partners and the like. While this license is intended to facilitate the commercial use of the Program, the Contributor who includes the Program in a commercial product offering should do so in a manner which does not create potential liability for other Contributors. Therefore, if a Contributor includes the Program in a commercial product offering, such Contributor (“Commercial Contributor”) hereby agrees to defend and indemnify every other Contributor (“Indemnified Contributor”) against any losses, damages and costs (collectively “Losses”) arising from claims, lawsuits and other legal actions brought by a third party against the Indemnified Contributor to the extent caused by the acts or omissions of such Commercial Contributor in connection with its distribution of the Program in a commercial product offering. The obligations in this section do not apply to any claims or Losses relating to any actual or alleged intellectual property infringement. In order to qualify, an Indemnified Contributor must: a) promptly notify the Commercial Contributor in writing of such claim, and b) allow the Commercial Contributor to control, and cooperate with the Commercial Contributor in, the defense and any related settlement negotiations. The Indemnified Contributor may participate in any such claim at its own expense.
 .
 For example, a Contributor might include the Program in a commercial product offering, Product X. That Contributor is then a Commercial Contributor. If that Commercial Contributor then makes performance claims, or offers warranties related to Product X, those performance claims and warranties are such Commercial Contributor's responsibility alone. Under this section, the Commercial Contributor would have to defend claims against the other Contributors related to those performance claims and warranties, and if a court requires any other Contributor to pay any damages as a result, the Commercial Contributor must pay those damages.
 5. NO WARRANTY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, THE PROGRAM IS PROVIDED ON AN “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Each Recipient is solely responsible for determining the appropriateness of using and distributing the Program and assumes all risks associated with its exercise of rights under this Agreement, including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and unavailability or interruption of operations.
 6. DISCLAIMER OF LIABILITY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, NEITHER RECIPIENT NOR ANY CONTRIBUTORS SHALL HAVE ANY LIABILITY FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION LOST PROFITS), HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OR DISTRIBUTION OF THE PROGRAM OR THE EXERCISE OF ANY RIGHTS GRANTED HEREUNDER, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 7. GENERAL
 .
 If any provision of this Agreement is invalid or unenforceable under applicable law, it shall not affect the validity or enforceability of the remainder of the terms of this Agreement, and without further action by the parties hereto, such provision shall be reformed to the minimum extent necessary to make such provision valid and enforceable.
 .
 If Recipient institutes patent litigation against any entity (including a cross-claim or counterclaim in a lawsuit) alleging that the Program itself (excluding combinations of the Program with other software or hardware) infringes such Recipient's patent(s), then such Recipient's rights granted under Section 2(b) shall terminate as of the date such litigation is filed.
 .
 All Recipient's rights under this Agreement shall terminate if it fails to comply with any of the material terms or conditions of this Agreement and does not cure such failure in a reasonable period of time after becoming aware of such noncompliance. If all Recipient's rights under this Agreement terminate, Recipient agrees to cease use and distribution of the Program as soon as reasonably practicable. However, Recipient's obligations under this Agreement and any licenses granted by Recipient relating to the Program shall continue and survive.
 .
 Everyone is permitted to copy and distribute copies of this Agreement, but in order to avoid inconsistency the Agreement is copyrighted and may only be modified in the following manner. The Agreement Steward reserves the right to publish new versions (including revisions) of this Agreement from time to time. No one other than the Agreement Steward has the right to modify this Agreement. The Eclipse Foundation is the initial Agreement Steward. The Eclipse Foundation may assign the responsibility to serve as the Agreement Steward to a suitable separate entity. Each new version of the Agreement will be given a distinguishing version number. The Program (including Contributions) may always be Distributed subject to the version of the Agreement under which it was received. In addition, after a new version of the Agreement is published, Contributor may elect to Distribute the Program (including its Contributions) under the new version.
 .
 Except as expressly stated in Sections 2(a) and 2(b) above, Recipient receives no rights or licenses to the intellectual property of any Contributor under this Agreement, whether expressly, by implication, estoppel or otherwise. All rights in the Program not expressly granted under this Agreement are reserved. Nothing in this Agreement is intended to be enforceable by any entity that is not a Contributor or Recipient. No third-party beneficiary rights are created under this Agreement.

License: WTFPL
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
  0. You just DO WHAT THE FUCK YOU WANT TO.
